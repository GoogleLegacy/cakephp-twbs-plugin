# CakePHP Twitter Bootstrap Plugin

Using twitter bootstrap 3.x with CakePHP helpers

## Installation

### Via Composer
Add `"lubos/twbs": "dev-master"` into your composer.json file. Make sure you have `"composer/installers": "*"` 
in your composer require as well. Run `composer update`.

## Demo

TBC

## Reference

TBC
