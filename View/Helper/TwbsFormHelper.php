<?php
App::uses('FormHelper', 'View/Helper');

class TwbsFormHelper extends FormHelper {

/**
 * The name of the helper
 *
 * @var string
 */
	public $name = 'TwbsForm';

/**
 * Right and left settings to have 2 columns (label and inputs) with an horizontal form
 * Default values are : left -> 3 ; right -> 9
 *
 * @var int
 */
	public $left = 3;
	public $right = 9;

/**
 * Defines the type of form being created, horizontal form or inline form. Set by Bootstrap3FormHelper::create()
 *
 * @var string
 */
	protected $_typeForm = 'horizontal';

/**
 * Defines the model of form being created. Set by Bootstrap3FormHelper::create()
 *
 * @var string
 */
	protected $_modelForm = null;

/**
 * Defines if use base cakephp FormHelper instead
 * 
 * @var mixed
 * @access protected
 */
	protected $_useStandardFormHelper = false;

/**
 * Sets left & right
 *
 * @param View $View The View this helper is being attached to.
 * @param array $settings Configuration settings for the helper.
 */
	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
		if ($left = Configure::read('Twbs.left')) {
			$this->left = $left;
		}
		if ($right = Configure::read('Twbs.right')) {
			$this->right = $right;
		}
	}

/**
 * Returns an HTML FORM element.
 *
 * Extends of FormHelper::create() so get same options and params
 *
 * Initialize the value of $_typeForm, $_modelForm and give attribute 'role' to the form')`
 *
 * @param mixed $model The model name for which the form is being defined.
 *   If an array is passed and $options argument is empty, the array will be used as options.
 *   If `false` no model is used.
 * @param array $options An array of html attributes and options.
 * @return string An formatted opening FORM tag.
 */

	public function create($model = null, $options = array()){

		if (isset($options['standardHelper'])){
			$this->_useStandardFormHelper = true;
		}

		if (!isset($options['role'])){
			$options['role'] = 'form';
		}

		if (isset($options['class'])) {
			if (is_integer(strpos($options['class'], 'form-horizontal'))){
				$this->_typeForm = 'horizontal';
			}elseif (is_integer(strpos($options['class'], 'form-inline'))) {
				$this->_typeForm = 'inline';
			}elseif (is_integer(strpos($options['class'], 'form-compact'))) {
				$this->_typeForm = 'compact';
			}
		}else{
			$options['class'] = 'form-horizontal';
		}

		if ($this->_typeForm == 'compact') {
			$this->left = 0;
			$this->right = 12;
		}

		$this->_modelForm = $model;

		return parent::create($model, $options);
	}

/**
 * Generates a form input element complete with label and wrapper div
 *
 * Extends of FormHelper::input() so get same options and params
 *
 * ### New Options
 *
 * - `state` - Change the state of the input to 'error', 'warning' or 'success'
 * - `help` - Add a message under the input to give more informations
 *
 * ### Options by default for Twitter Bootstrap v3
 *
 * - 'before' - Add a 'form-group' div
 * - 'between' - Open the right column of the form
 * - 'after' - Close div tags
 * - 'div' - Set to 'false'
 * - 'class' - Add 'form-control'
 * - 'label' - Add a 'control-label' class and if the form is horizontal, put the label into the left column of it
 *
 * ### Options can be extends with no conflicts when the input is being created
 *
 * - 'class'
 * - 'label' - string and array
 *
 * @param string $fieldName This should be "Modelname.fieldname"
 * @param array $options Each type of input takes different options.
 * @return string Completed form widget.
 */
	public function input($fieldName, $options = array()){

		if ($this->_useStandardFormHelper) {
			return parent::input($fieldName, $options);
		}
		list(, $fieldName) = pluginSplit($fieldName);

		$model = $this->model(); 
		$errors = array();
		if (!empty($model) && isset($this->validationErrors[$model])) {
			$errors = $this->validationErrors[$model];
		}

		if (isset($options['type']) && $options['type'] == 'checkbox') {
			return $this->checkbox($fieldName, $options);
		}

		//----- [before], [state] and [after] options
		if (!isset($options['before']) && !isset($options['after'])) {
			$before = false;
			if (isset($options['state'])) {
				switch ($options['state']) {
					case 'error':
						$state = 'has-error';
						break;
					case 'warning':
						$state = 'has-warning';
						break;
					case 'success':
						$state = 'has-success';
						break;
					
					default:
						$state = '';
						break;
				}
				$before = array('form-group');
				if (!(isset($options['label']) && $options['label'] === false)) {
					$before[] = $state;
				}
			} else if ((!empty($errors)) && in_array($fieldName, array_keys($errors))) {
				$before = array('form-group has-error');
				$options['help'] = '';
			}else{
				$before = array('form-group');
			}
			if ($before) {
				// check if required
				if ($this->_introspectModel($model, 'validates', $fieldName)) {
					$before[] = 'required';
				};
				$options['before'] = sprintf('<div class="%s">', implode(' ', $before)); 
			}
			$options['after'] = '</div>';
		}

		//----- [div] option
		if (!isset($options['div'])) {
			$options['div'] = false;
		}

		//----- [class] option
		if (isset($options['class'])) {
			$options['class'] .= ' form-control';
		}else{
			$typeBlacklist = array('file');
			$multipleBlacklist = array('checkbox');
			if (!(
				isset($options['type']) && in_array($options['type'],  $typeBlacklist) || 
				isset($options['multiple']) && in_array($options['multiple'],  $multipleBlacklist)
			)) {
				$options['class'] = 'form-control';
			}
		}

		//----- [label] option
		if (isset($options['label']) && $options['label'] === false) {
		} elseif (!isset($options['label'])) {
			if ($this->_typeForm == 'horizontal' && $this->left) {
				$options['label'] = array('class' => 'control-label col-md-'.$this->left);
			}else{
				$options['label'] = array('class' => 'control-label');
			}
		}else{
			if (!is_array($options['label'])) {
				if ($this->_typeForm == 'horizontal' && $this->left) {
					$options['label'] = array('class' => 'control-label col-md-'.$this->left, 'text' => $options['label']);
				} else {
					$options['label'] = array('class' => 'control-label', 'text' => $options['label']);
				}
			}else{
				if ($this->_typeForm == 'horizontal' && $this->left) {
					if (isset($options['label']['class'])) {
						$options['label']['class'] .= ' control-label col-md-'.$this->left;
					}else{
						$options['label']['class'] = 'control-label col-md-'.$this->left;
					}
				} else {
					if (isset($options['label']['class'])) {
						$options['label']['class'] .= ' control-label';
					}else{
						$options['label']['class'] = 'control-label';
					}
				}
			}
		}

		//----- [between], [after] and [help] options
		if ($this->_typeForm == 'horizontal') {
			$options['after'] = '';
			if ($this->right) {
				$options['between'] = '<div class="col-md-'.$this->right.'">';
			}
			if (isset($options['help']) && !empty($options['help'])) {
				if (isset($options['type']) && $options['type'] == 'radio') {
					$options['before'] .= sprintf(
						'<span class="help-block help-radio col-md-offset-%s col-md-%s">%s</span>',
						$this->left,
						$this->right,
						$options['help']
					);
				} else {
					$options['after'] .= '<span class="help-block">'.$options['help'].'</span>';
				}
			}
			if ($this->right) {
				$options['after'] .= '</div>';
			}
			$options['after'] .= '</div>';
		}

		if (!isset($options['format'])){
			$options['format'] = array('before', 'label', 'between', 'input', 'error', 'after');
		}

		if (isset($options['help'])) {
			unset($options['help']);
		}

		return parent::input($fieldName, $options);
	}

	public function error($field, $text = null, $options = array()) {

		if ($this->_useStandardFormHelper) {
			return parent::error($field, $text, $options);
		}

		$defaults = array('wrap' => 'span', 'class' => 'help-block');
		$options = array_merge($defaults, $options);
		return parent::error($field, $text, $options);
	}

	public function checkbox($fieldName, $options = array()){

		if ($this->_useStandardFormHelper) {
			return parent::checkbox($fieldName, $options);
		}

		$model = $this->model();

		if (!empty($model) && isset($this->validationErrors[$model])) {
			$errors = $this->validationErrors[$model];
		}
		$error = '';
		$class = 'form-group';
		if (isset($errors[$fieldName])) {
			$error = $this->Html->tag('span', $errors[$fieldName][0], array('class' => 'help-block'));
			$class .= ' has-error';
		}
		
		$out = parent::checkbox($fieldName, $options);
		$options = $this->_initInputField($fieldName, $options);
		$out = $this->Html->div('checkbox', implode(array(
			$this->Html->tag(
				'label',
				sprintf('%s %s', $out, $options['label']),
				array('for' => $options['id'])
			),
			$error
		)));
		if ($this->_typeForm == 'horizontal') {
			if ($this->left && $this->right) {
				$out = $this->Html->div(sprintf('col-md-offset-%s col-md-%s', $this->left, $this->right), $out);
			} elseif ($this->right) {
				$out = $this->Html->div(sprintf('col-md-%s', $this->right), $out);
			}
			$out = $this->Html->div($class, $out);
		}

		return $out;
	}

/**
 * Returns a formatted SELECT element.
 *
 * Extends of FormHelper::select() so get same attributes and params
 *
 * ### New Attributes:
 *
 * - `inline` - Only when attribute [multiple] is checkbox. Align all the checkboxes
 * - `help` - Add a message under the select element to give more informations
 * - 'label' - Add a label to the select element
 *
 * @param string $fieldName Name attribute of the SELECT
 * @param array $options Array of the OPTION elements (as 'value'=>'Text' pairs or as array('label' => 'text', 'help' => 'informations') in case of multiple checkbox) to be used in the
 *	SELECT element
 * @param array $attributes The HTML attributes of the select element.
 * @return string Formatted SELECT element
 */

	public function select($fieldName, $options = array(), $attributes = array()){

		$out = '';

		// MULTIPLE CHECKBOX			
		if (isset($attributes['multiple']) && $attributes['multiple'] === 'checkbox') {
			//----- [inline] attribute for checkbox
			if(isset($attributes['inline']) && ($attributes['inline'] == 'inline' || $attributes['inline'] == true)){
				if (!isset($attributes['class'])) {
					$attributes['class'] = 'checkbox checkbox-inline';
				}else{
					$attributes['class'] = 'checkbox checkbox-inline '.$attributes['class'];
				}
			}else{
				if (!isset($attributes['class'])) {
					$attributes['class'] = 'checkbox';
				}else{
					$attributes['class'] = 'checkbox '.$attributes['class'];
				}
			}
		}

		//----- [empty] attribute
		if (!isset($attributes['empty'])) {
			$attributes['empty'] = false;
		}

		if ($this->_typeForm == 'horizontal') {
			//----- [label] attribute
			if (isset($attributes['label']) && !empty($attributes['label'])) {
				if ($this->left) {
					$out .= '<label class="control-label col-md-'.$this->left.'">'.$attributes['label'].'</label>';
				} else {
					$out .= '<label class="control-label>'.$attributes['label'].'</label>';
				}
				if ($this->right) {
					$out .= '<div class="col-md-'.$this->right.'">';
				}
			}
		}

		$out .= parent::select($fieldName, $options, $attributes);

		if ($this->_typeForm == 'horizontal') {
			//----- [help] attribute
			if (isset($attributes['help']) && !empty($attributes['help'])) {
				$out .= '<span class="help-block">'.$attributes['help'].'</span>';
			}
			if (isset($attributes['label']) && !empty($attributes['label'])) {
				if ($this->right) {
					$out .= '</div>';
				}
			}
		}

		return $out;
	}

/**
 * Creates a set of radio widgets.
 *
 * ### Attributes:
 *
 * - `label` - Create a label element for the radio buttons in the left column
 * - `value` - indicate a value that is should be checked
 * - `disabled` - Set to `true` or `disabled` to disable all the radio buttons.
 * - `help` - Add a message under the radio buttons to give more informations
 * - `inline` - Align all the radio buttons
 *
 * @param string $fieldName Name of a field, like this "Modelname.fieldname"
 * @param array $options Radio button options array (as 'value'=>'Text' pairs or as array('label' => 'text', 'help' => 'informations')
 * @param array $attributes Array of HTML attributes, and special attributes above.
 * @return string Completed radio widget set.
 */

	public function radio($fieldName, $options = array(), $attributes = array()){

		if ($this->_useStandardFormHelper) {
			return parent::radio($fieldName, $options, $attributes);
		}
		
		$out = '';

		if ($this->_typeForm == 'horizontal') {

			//----- [label] attribute
			if (isset($attributes['label']) && !empty($attributes['label'])) {
				if (!is_array($attributes['label'])) {
					$attributes['label'] = array(
						'text' => $attributes['label']
					);
				}
				$label = $attributes['label']['text'];
				if ($this->left) {
					$out .= '<label class="control-label col-md-'.$this->left.'">'.$label.'</label>';
				} else {
					$out .= '<label class="control-label>'.$label.'</label>';
				}
				if ($this->right) {
					$out .= '<div class="col-md-'.$this->right.'">';
				}
			}else{
				if ($this->left && $this->right) {
					$out .= '<div class="col-md-offset-'.$this->left.' col-md-'.$this->right.'">';
				} elseif ($this->right) {
					$out .= '<div class="col-md-'.$this->right.'">';
				}
			}
		}

		foreach ($options as $key => $value) {

			//----- [inline] attribute
			if (!(isset($attributes['inline']) && ($attributes['inline'] == 'inline' || $attributes['inline'] == true))) {
				$out .= '<div class="radio">';
				$out .= '<label>';
			}else{
				$out .= '<label class="radio-inline">';
			}

			$out .= '<input type="radio" name="data['.$this->_modelForm.']['.$fieldName.']" id="'.$this->_modelForm.Inflector::camelize($fieldName).$key.'" value="'.$key.'"';

			//----- [value] attribute
			if (isset($attributes['value']) && $attributes['value'] == $key) {
				$out .= ' checked';
			}

			//----- [disabled] attribute
			if (isset($attributes['disabled'])){
				if (!is_array($attributes['disabled'])){
					if ($attributes['disabled'] == true || $attributes['disabled'] == 'disabled') {
						$out .= ' disabled';
					}
				}else{
					foreach ($attributes['disabled'] as $elem) {
						if ($elem == $key) {
							$out .= ' disabled';
						}
					}
				}
			}

			// If options are array('label' => 'text')
			if (is_array($value)) {
				$out .= '>'.' '.$value['label'];
			}else{
				$out .= '>'.' '.$value;
			}

			$out .= '</label>';

			//----- [help] option
			if (isset($value['help']) && !empty($value['help']) && is_array($options[$key])) {
				$out .= '<span class="help-block">'.$options[$key]['help'].'</span>';
			}

			//----- [inline] attribute
			if (!(isset($attributes['inline']) && ($attributes['inline'] == 'inline' || $attributes['inline'] == true))) {
				$out .= '</div>';
			}
		}

		if ($this->_typeForm == 'horizontal') {

			//----- [help] attribute
			if (isset($attributes['help']) && !empty($attributes['help'])) {
				$out .= '<span class="help-block">'.$attributes['help'].'</span>';
			}
		}

		return $out;
	}

/**
 * Creates a submit button element.
 *
 * Extends of FormHelper::submit() so get same options and params
 *
 * ### Options by default for Twitter Bootstrap v3
 *
 * - `div` - Set to 'false'
 * - 'label' - Set to 'false'
 * - 'class' - Set into a green button instead of a default button
 *
 * @param string $caption The label appearing on the button OR if string contains :// or the
 *  extension .jpg, .jpe, .jpeg, .gif, .png use an image if the extension
 *  exists, AND the first character is /, image is relative to webroot,
 *  OR if the first character is not /, image is relative to webroot/img.
 * @param array $options Array of options. See above.
 * @return string A HTML submit button
 */

	public function submit($caption = null, $options = array()){

		if ($this->_useStandardFormHelper) {
			return parent::submit($caption, $options);
		}
		
		$out = '';

		if ($this->_typeForm == 'horizontal') {
			$out .= '<div class="form-group">';
			if ($this->left && $this->right) {
				$out .= '<div class="col-md-offset-'.$this->left.' col-md-'.$this->right.'">';
			} elseif ($this->right) {
				$out .= '<div class="col-md-'.$this->right.'">';
			}
		}
		
		//----- [div] option
		if (!isset($options['div'])) {
			$options['div'] = false;
		}

		//----- [label] option
		if (!isset($options['label'])) {
			$options['label'] = false;
		}

		//----- [class] option
		if (!isset($options['class'])) {
			$options['class'] = 'btn btn-success';
		}else{
			$options['class'] = 'btn '.$options['class'];
		}

		$out .= parent::submit($caption, $options);

		if ($this->_typeForm == 'horizontal') {
			if ($this->right) {
				$out .= '</div>';
			}
			$out .= '</div>';
		}

		return $out;
	}

/**
 * Closes an HTML form, cleans up values set by Bootstrap3FormHelper::create(), and writes hidden
 * input fields where appropriate.
 *
 * @param string|array $options as a string will use $options as the value of button,
 * @return string a closing FORM tag optional submit button.
 */

	public function end($options = null, $secureAttributes = array()){
		return parent::end($options, $secureAttributes);
	}

	public function setTypeForm($formType) {
		$this->_typeForm = $formType;
		return $this;
	}
}
