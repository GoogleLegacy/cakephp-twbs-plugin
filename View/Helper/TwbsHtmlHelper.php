<?php
App::uses('HtmlHelper', 'View/Helper');

class TwbsHtmlHelper extends HtmlHelper {

	public function button($label, $options = array()) {
		extract($options);
		$class = '';
		if (!empty($options['class'])) {
			$class = $options['class'];
		}
		$class = sprintf('btn %s', $class);
		if (!isset($type)) {
			$type = 'btn-default';
		}
		$class .= sprintf(' %s', $type);
		$options['class'] = $class;
		if (is_array($label)) {
			$title = $label[0];
			$url = null;
			if (!empty($label[1])) {
				$url = $label[1];
			}
			if (!empty($label[2])) {
				$buttonClass = array();
				if (!empty($options['class'])) {
					$buttonClass[] = $options['class'];
				}
				if (!empty($label[2]['class'])) {
					$buttonClass[] = $label[2]['class'];
				}
				$options = array_merge($options, $label[2]);
				if ($buttonClass) {
					$options['class'] = implode(' ', $buttonClass);
				}
			}
			if (!empty($options['icon'])) {
				$title = sprintf('%s %s', $this->icon($options['icon']), $title);
				$options['escape'] = false;
				unset($options['icon']);
			}
			$confirmMessage = false;
			if (!empty($label[3])) {
				$confirmMessage = $label[3];
			}
			$out = $this->link($title, $url, $options, $confirmMessage);
		} else {
			$out = $this->tag('button', $label, $options);
		}
		return $out;
	}

	public function buttonToolbar($data = array(), $options = array()) {
		$out = array();
		foreach ($data as $group) {
			if (is_array($group)) {
				$out[] = $this->buttonGroup($group, $options);
			} else {
				$out[] = $group;
			}
		}
		return $this->tag('div', implode('', $out), array('class' => 'btn-toolbar', 'role' => 'toolbar'));
	}

	public function buttonGroup($data = array(), $defaults = array()) {
		$out = array();
		foreach ($data as $item) {
			$options = $defaults;
			if (is_array($item)) {
				list($item, $options) = $item;
			}
			$out[] = $this->button($item, $options);
		}
		return $this->tag('div', implode('', $out), array('class' => 'btn-group'));
	}

	public function buttonDropdown($data = array(), $options = array()) {
		$out = array();
		if (empty($options['class'])) {
			$options['class'] = '';
		}
		$align = '';
		if (!empty($options['align'])) {
			$align = $options['align'];
			unset($options['align']);
		}
		$type = false;
		$button = $data[0];
		if (!empty($options['type']) && $options['type'] == 'open') {
			$type = 'open';
			unset($options['open']);
			$options['class'] .= ' dropdown-toggle';
			$options['data-toggle'] = 'dropdown';
			unset($data[0]);
			$button[0] .= sprintf(' %s', $this->tag('span', '', array('class' => 'caret')));
		}
		$out[] = $this->button($button, $options);
		if ($type === false) {
			$options['class'] .= ' dropdown-toggle';
			$options['data-toggle'] = 'dropdown';
			$out[] = $this->button(implode('', array(
				$this->span('caret', ''),
				$this->span('sr-only', 'Toggle Dropdown'),
			)), $options);
		}
		$options = array('class' => sprintf('dropdown-menu %s', $align));
		$out[] = $this->dropdownMenu($data, $options);
		return $this->div('btn-group', implode('', $out));
	}

	public function dropdownMenu($data = array(), $options = array()) {
		$defaultOptions = array('class' => 'dropdown-menu', 'role' => 'menu');
		$options = array_merge($defaultOptions, $options);
		$out = array();
		foreach ($data as $item) {
			$linkTitle = $item[0];
			$linkOptions = array();
			if (!empty($item[2])) {
				$linkOptions = $item[2];
			}
			if (!empty($linkOptions['icon'])) {
				$linkTitle = sprintf('%s %s', $this->icon($linkOptions['icon']), $linkTitle);
				$linkOptions['escape'] = false;
				unset($linkOptions['icon']);
			}
			$confirmMessage = false;
			if (!empty($item[3])) {
				$confirmMessage = $item[3];
			}
			$link = $this->link($linkTitle, $item[1], $linkOptions, $confirmMessage);
			$out[] = $this->tag('li', $link);
		}
		return $this->tag('ul', implode('', $out), $options);
	}

	public function icon($class, $url = array(), $options = array()) {
		$output = array();
		$output[] = $this->tag('i', '', array('class' => $class));
		if (!empty($options['label'])) {
			$output[] = $this->tag('span', sprintf(' %s', h($options['label'])));	
			unset ($options['label']);
		}
		$output = implode('', $output);
		if (!empty($url)) {
			$options = array_merge(array('escape' => false), $options);
			$output = $this->link($output, $url, $options);
		}
		return $output;
	}

	public function span($class = null, $text = '', $options = array()) {
		if ($class) {
			$options['class'] = $class;
		}
		return $this->tag('span', $text, $options);
	}

	public function close($options = array(), $label = '&times;') {
		$defaults = array('type' => false, 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true');
		$options = array_merge($defaults, (array)$options);
		$out = $this->button($label, $options);
		return $out;
	}

	public function modal($id, $body, $header = null, $footer = null, $options = array()) {
		$defaults = array('class' => 'modal fade', 'tabindex' => '-1', 'role' => 'dialog', 'aria-hidden' => true);
		$options = array_merge($defaults, (array)$options);
		$options['id'] = $id;
		$out = '';
		if ($header) {
			$out .= $this->div('modal-header', $header);
		}
		$out .= $this->div('modal-body', $body);
		if ($footer) {
			$out .= $this->div('modal-footer', $footer);
		}
		$out = $this->div('modal-content', $out);
		$out = $this->div('modal-dialog', $out);
		$out = $this->div($options['class'], $out, $options);
		return $out;
	}
}
