<?php 
if (empty($class)) {
	$class = 'alert alert-success';
}
$button = $this->Html->tag(
	'button', '&times;', 
	array('class' => 'close', 'data-dismiss' => 'alert', 'aria-hidden' => 'true')
);
echo $this->Html->div($class, $button.$message, array('id' => 'flash'));
