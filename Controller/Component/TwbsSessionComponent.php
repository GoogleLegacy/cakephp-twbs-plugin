<?php
App::uses('SessionComponent', 'Controller/Component');

class TwbsSessionComponent extends SessionComponent {

	public function flash($message, $type = 'success', $options = array()) {
		$element = sprintf('Twbs.flash');
		$class = sprintf('alert alert-%s alert-dismissable', $type);
		if (!empty($options['class'])) {
			$class = sprintf('%s %s', $class, $options['class']);
		}
		$params = array('class' => $class);
		$key = 'flash';
		return parent::setFlash($message, $element, $params, $key);
	}
}
