<?php
App::uses('Component', 'Controller');
class TwbsComponent extends Component {

	public function initialize(Controller $controller) {
		// load helpers
		$controller->helpers['Html'] = array('className' => 'Twbs.TwbsHtml');
		$controller->helpers['Form'] = array('className' => 'Twbs.TwbsForm');
	}

	public function beforeRender(Controller $controller) {
		// append view paths
		$views = array(
			APP . 'View' . DS,
			CakePlugin::path('Twbs') . 'View' . DS
		);
		App::build(array('views' => $views));
	}
}
